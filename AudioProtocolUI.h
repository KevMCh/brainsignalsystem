#ifndef AUDIOPROTOCOLUI_H
#define AUDIOPROTOCOLUI_H

#include "StimuliProtocolUI.h"

class AudioProtocolUI : public StimuliProtocolUI {

    public:
        explicit AudioProtocolUI();
        ~AudioProtocolUI();
};

#endif // AUDIOPROTOCOLUI_H
