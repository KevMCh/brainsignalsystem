#ifndef STIMULUSMENUUI_H
#define STIMULUSMENUUI_H

#include <QWidget>
#include <QScrollArea>

#include "StimuliRenderingUI.h"
#include "GeneralDataUI.h"
#include "ProtocolsUI.h"
#include "ControlPanelStimuliScreenUI.h"
#include "StimuliRenderingUI.h"
#include "DataLogUI.h"

class StimuliMenuUI : public QWidget {
    Q_OBJECT

    private:
        GeneralDataUI* generalDataUI;
        ProtocolsUI* protocolsUI;
        ControlPanelStimuliScreenUI* controlPanelStimuliScreenUI;
        DataLogUI* dataLogUI;

        QScrollArea* scrollDataLog;

    public:
        explicit StimuliMenuUI(QWidget *parent = nullptr);
        ~StimuliMenuUI();

        void closeEvent(QCloseEvent *event);

        GeneralDataUI* getGeneralDataUI() const;
        void setGeneralDataUI(GeneralDataUI* value);

        ProtocolsUI* getProtocolsUI() const;
        void setProtocolsUI(ProtocolsUI* value);

        ControlPanelStimuliScreenUI* getControlPanelStimuliScreenUI() const;
        void setControlPanelStimuliScreenUI(ControlPanelStimuliScreenUI* value);

        DataLogUI* getDataLogUI() const;
        void setDataLogUI(DataLogUI* value);

        QScrollArea* getScrollDataLog() const;
        void setScrollDataLog(QScrollArea* value);

    private:
            void createContent();
};

#endif // STIMULUSMENUUI_H
