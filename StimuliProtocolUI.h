#ifndef STIMULIPROTOCOLUI_H
#define STIMULIPROTOCOLUI_H

#include <QWidget>

#include "ProtocolUI.h"

class StimuliProtocolUI : public ProtocolUI {

    private:
        QSpinBox* numberStimuli;
        QSpinBox* stimuliTime;
        QSpinBox* timeBetweenStimuli;

    public:
        StimuliProtocolUI();
        ~StimuliProtocolUI();

        int getNumberStimuliSelected();
        int getStimuliTimeSelected();
        int getTimeBetweenStimuliSelected();

        QSpinBox* getNumberStimuli() const;
        void setNumberStimuli(QSpinBox *value);

        QSpinBox* getStimuliTime() const;
        void setStimuliTime(QSpinBox *value);

        QSpinBox* getTimeBetweenStimuli() const;
        void setTimeBetweenStimuli(QSpinBox *value);

    private:
        void createNumberOfStimuliInput();
        void createStimulusTimeInput();
        void createTimeBetweenStimuliInput();
};

#endif // STIMULIPROTOCOL_UI_H
